<?php
define('USE_AUTHENTICATION', 1);
define('USERNAME', 'epicor');
define('PASSWORD', 'epicor');

if ( USE_AUTHENTICATION == 1 ) {
  if ( !isset($_SERVER['PHP_AUTH_USER'] ) || !isset( $_SERVER['PHP_AUTH_PW'] ) ||
  $_SERVER['PHP_AUTH_USER'] != USERNAME || $_SERVER['PHP_AUTH_PW'] != PASSWORD ) {
    header( 'WWW-Authenticate: Basic realm="EPICOR Log In!"' );
    header( 'HTTP/1.0 401 Unauthorized' );
    exit;
  }
  else
  {
    $myObj->name = "John";
    $myObj->age = 30;
    $myObj->city = "New York";

    $myJSON = json_encode($myObj);

    echo $myJSON;
  }
}
?>
