<?php

class Kendaraan{
  protected $jenis;

  public function setJenis($input){
    $this->jenis = $input;
  }

  protected function getJenis(){
    return $this->jenis;
  }
}

class Mobil extends Kendaraan{

  protected $merk;
  public static $harga;

  public function setMerk($input){
    $this->merk = $input;
  }

  public function getMerk(){
    return $this->merk;
  }

  public function getAll(){
    echo $this->getJenis();
    echo "<br>";
    echo $this->getMerk();
    echo self::$harga;
  }

  public static function getHarga(){
    return self::$harga;
  }

}

$honda = new Mobil();
$honda->setMerk("Honda");
$honda->setJenis("Mobil");
Mobil::$harga = 10000;
echo Mobil::getHarga();
echo "<br>";
echo $honda->getAll();

//notes
//public = bisa diakses di luar kelas, dalam kelas, dan turunannya
//protected = bisa diakses di dalam kelas dan turunannya
//private = bisa diakses hanya di dalam kelas

//static = bisa dipanggil dengan cara :: dan semuanya harus static
//tidak boleh ada object di dalam method yg static, tapi boleh ada static dalam method objek
//kalau ingin return bisa menggunakan self::$variable atau parent::$variable (kalau posisinya ada di parent)


 ?>
