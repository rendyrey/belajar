<?php
class mahasiswa extends manusia{
    var $nama = '';
    var $jurusan = '';
    var $jenis_kelamin = '';

    

    function set_nama($nama){
        $this->nama = $nama;
    }

    function set_jurusan($jurusan){
        $this->jurusan = $jurusan;
    }

    function set_jenis_kelamin($jenis_kelamin){
        $this->jenis_kelamin = $jenis_kelamin;
    }

    function get_nama(){
        return $this->nama;
    }

    function get_jurusan(){
        return $this->jurusan;
    }

    function get_jenis_kelamin(){
        return $this->jenis_kelamin;
    }

    protected function get_all(){
        return $this->nama." ".$this->jurusan." ".$this->jenis_kelamin;
    }

    function get_mhs(){
        return $this->get_all();
    }

    function get_ras(){
        return $this->get_manusia();
    }


    
}

class manusia{
    var $ras = "";
    var $suku = "";
    var $agama = "";
    
    protected function get_manusia(){
        return $this->ras;
    }

}

?>