<?php
  class Komputer{
    protected $merk = '';
    protected $harga = '';
    protected $tipe = 'Ultrabook';
    protected $status = 'lunas';

    public function setMerk($merk){
      $this->merk = $merk;
    }

    public function getMerk(){
      return $this->merk;
    }

    public function setHarga($harga){
      $this->harga = $harga;
    }

    public function getHarga(){
      return $this->harga;
    }

    protected function getAll(){
      echo $this->merk." ".$this->harga." ".$this->tipe;
    }


  }

  class Laptop extends Komputer{
    public static $tipe_laptop = 'Tipe Laptop Static';

    public $coba = 'cobain';

    public function setTipeLaptop($tipe){
      $this->tipe = $tipe;
    }

    public static function getTipeLaptop(){
      return self::$tipe_laptop;
    }

    public function getTL(){
      return $this->tipe;
    }

    public function getCoba(){
      return $this->coba;
    }

    public function getText(){
      return $this->getAll();
    }


  }

  $asus = new Laptop();
  $asus->setMerk("Asus");
  $asus->setHarga("12.400.000");
  $asus->setTipeLaptop("Ultrabook Fast");


  echo $asus->getText();

  echo "<br>";
  echo Laptop::$tipe_laptop;
  echo "<br>";
  Laptop::$tipe_laptop = "Tipe Laptop Baru";
  echo Laptop::getTipeLaptop();





?>
