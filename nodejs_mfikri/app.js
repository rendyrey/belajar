// const http = require('http');
// const express = require('express');
// const app = express();

// // http.createServer(function(req,res){
// //     res.writeHead(200,{
// //         "Content-type":"text/html"
// //     });
// //     res.end("Hello, World!");
// // }).listen(8000);

// // console.log("Server is running at port 8000");

// app.get('/',function(req,res){
//     res.send("Welcome to express");
// });

// app.get('/about',function(req,res){
//     res.send("This is about page");
// })


// app.listen(8000,function(){
//     console.log("Server is running at port 8000");
// });

//use path module
const path = require('path');
//use express module
const express = require('express');
//use hbs view engine
const hbs = require('hbs');
const app = express();

//set dynamic views file
app.set('views',path.join(__dirname,'views'));
//set view engine
app.set('view engine', 'hbs');
//set public folder as static folder for static file
app.use(express.static('public'));
//route untuk halaman home
app.get('/',(req, res) => {
  //render file index.hbs
  data = {
    name:"Rendy Reynaldy"
  }
  res.render('index',data);
});

app.get('/:name',(req,res)=>{
  res.render('index',{
    name:req.params.name
  });
})

//route untuk halaman about
app.get('/about',(req, res) => {
  res.send('This is about page');
});

app.listen(8000, () => {
  console.log('Server is running at port 8000');
  // console.log(__dirname);
});
